package com.onlinequiz.onlinequizapp.security;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;

public class SecurityConstants {
    public static final long JWT_EXPIRATION =70000;

//    public static final String JWT_SECERET= "seceret";


    public static final String SECRET_KEY = System.getenv("JWT_SECRET");


    //    public static final SecretKey key = Keys.hmacShaKeyFor(SecurityConstants.JWT_SECERET.getBytes());
    public static final SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

}
