package com.onlinequiz.onlinequizapp.repository;

import com.onlinequiz.onlinequizapp.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface QuestionRepository extends JpaRepository<Question,Long> {
    Optional<Question> findById(long id);
}
