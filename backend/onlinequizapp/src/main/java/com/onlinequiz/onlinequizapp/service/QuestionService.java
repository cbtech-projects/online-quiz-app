package com.onlinequiz.onlinequizapp.service;

import com.onlinequiz.onlinequizapp.dto.QuestionDto;
import com.onlinequiz.onlinequizapp.models.Question;

import java.util.List;

public interface QuestionService {
    List<QuestionDto> findAllQuestions();
}
