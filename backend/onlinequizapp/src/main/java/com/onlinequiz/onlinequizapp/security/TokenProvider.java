package com.onlinequiz.onlinequizapp.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
public class
TokenProvider {

    public String generateToken(Authentication authentication){
        String username = authentication.getName();
        Date currentDate = new Date();
        Date expireDate = new Date(currentDate.getTime() + SecurityConstants.JWT_EXPIRATION);


        String token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
//                .signWith(SecurityConstants.key,SignatureAlgorithm.ES512)
                .signWith(SecurityConstants.key,SignatureAlgorithm.HS256)
                .compact();
        return token;
    }
    public String getUsernameFromJwt(String token){
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(SecurityConstants.key)
                .build()
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }

    public boolean validateToken(String token){
        try{
            Jwts.parserBuilder().setSigningKey(SecurityConstants.key).build().parseClaimsJws(token);
            return true;
        }catch (Exception ex){
            throw  new AuthenticationCredentialsNotFoundException("JWT was expired or incorrect");
        }
    }

}
