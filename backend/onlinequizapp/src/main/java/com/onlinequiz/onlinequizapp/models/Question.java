package com.onlinequiz.onlinequizapp.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;

    private String type; // (true or false,multiple choice)

    private String options;

    private String answer;

    @ManyToOne
    @JoinColumn(name="quiz_id")
    private Quiz quiz;

}
