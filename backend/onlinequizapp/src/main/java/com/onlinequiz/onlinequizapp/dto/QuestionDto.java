package com.onlinequiz.onlinequizapp.dto;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class QuestionDto  {
    private Long id;
    private String text;
    private String type; // (true or false,multiple choice)
    private String options;
    private String answer;
}
