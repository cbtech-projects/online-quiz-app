package com.onlinequiz.onlinequizapp.service.imp;

import com.onlinequiz.onlinequizapp.dto.QuestionDto;
import com.onlinequiz.onlinequizapp.models.Question;
import com.onlinequiz.onlinequizapp.repository.QuestionRepository;
import com.onlinequiz.onlinequizapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class QuestionServiceImpl implements QuestionService {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }


    @Override
    public List<QuestionDto> findAllQuestions() {
        List<Question> questions = questionRepository.findAll();
        return questions.stream()
                .map(this::mapToQuestionDto)
                .collect(Collectors.toList());    }


    public QuestionDto mapToQuestionDto(Question question){
        QuestionDto questionDto = QuestionDto.builder()
                .id(question.getId())
                .text(question.getText())
                .type(question.getType())
                .options(question.getOptions())
                .answer(question.getAnswer())
                .build();
        return questionDto;
    }
}
