package com.onlinequiz.onlinequizapp.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name="schedule")
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    private Date scheduleDate;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

    private boolean autoPublish; // Indicates if the quiz should be auto-published

}
