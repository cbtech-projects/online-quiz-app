import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InstructorDashboardComponent } from './instructor-dashboard/instructor-dashboard.component';
import { QuizCreationComponent } from './quiz-creation/quiz-creation.component';
import { LoginComponent } from './signin/login.component';
import { RegisterComponent } from './signup/register.component';

const routes: Routes = [
  {  path:'**',component:LoginComponent},
  {  path:'instructordboard',component:InstructorDashboardComponent},
  {  path:'quizcreation',component:QuizCreationComponent},
  {  path:'signin',component:LoginComponent},
  {  path:'signup',component:RegisterComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
