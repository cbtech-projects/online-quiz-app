import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
// import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
    username: string= "";
    password: String="";

  constructor(private router:Router,private http:HttpClient) {

   }

  login(){
    let body={
    username: this.username,
    password:this.password
    };

    this.http.post("http://localhost:8080/api/auth/login",body).subscribe((resultData:any)=>{
      console.log(resultData);
      alert("logged in Successfuly")
    })
  }
}

