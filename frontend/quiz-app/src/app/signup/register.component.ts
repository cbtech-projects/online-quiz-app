import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  userForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient
  ) {
    this.userForm = this.createForm();
  }

  signup() {
    if (this.userForm.valid) {
      const bodyData = {
        username: this.userForm.get('name')?.value,
        email: this.userForm.get('email')?.value,
        password: this.userForm.get('pass')?.value
      };

      this.http.post('http://localhost:8080/api/auth/register', bodyData, { responseType: 'text' }).subscribe((resultData: any) => {
        console.log(resultData);
        alert('Registered Successfully');
        this.router.navigate(['/login']);
      });
    }
  }

  private createForm(): FormGroup {
    return this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[A-Z][a-z]{2,24}$')]],
      email: ['', [Validators.required, Validators.email]],
      pass: ['', [
        Validators.required,
        Validators.pattern('^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$')
      ]],
      conpass: ['', [Validators.required]]
    }, { validators: this.passwordMatchValidator });
  }

  private passwordMatchValidator: ValidatorFn = (group: AbstractControl): { [key: string]: any } | null => {
    const pass = group.get('pass')?.value;
    const conpass = group.get('conpass')?.value;
    return pass === conpass ? null : { mismatch: true };
  };
}
